# 这是啥?

这是一款扩展了 layui 功能的插件, 能让你的输入框可以变成一个 `数字输入框`, 当一个普通的输入框变成了 `数字输入框` 后, 它就可以完成更多实用的操作了

![场景配图](./imgs/example.png)

---

# 怎么使用?

1. 下载源代码, 通常情况下, 你可以在 [Releases](https://gitee.com/layui-exts/number-input/releases) 这里找到所有已经发布的版本.
2. 将下载好的文件, 通常是压缩包, 解压到你项目的扩展目录里去, 譬如: `libs/layui_exts`
3. 确认项目的 `layui.config` 和 `layui.base` 配置是否正确, 可参考 [示例文件](./example.html)
4. 使用 `layui.use` 来引入扩展! 可参考 [示例文件](./example.html)

---

# 避雷指南

!> 输入框能输入什么类型的数据扩展已经做了限制, 并不推荐你使用 `input[type="number"]` 这样的方式来指定这是一个 `数字输入框`

!> 如果你确定要写成 `input[type="number"]`, 那么请注意! 你可能需要自己额外的处理一些事件, 如: `mousewheel` 鼠标滚轮滚动 或 `keypress` 当按下了某个键!

---

# 代码片段

> 仅供学习参考, 请勿无脑复制粘贴照搬照抄.

## 配置扩展

```javascript
layui
    .config({
        // 配置扩展的根目录
        base: "./libs/layui_exts/",
    })
    .extend({
        // 做好映射关系
        numberInput: "numberInput/js/index",
    });
```

## 引入扩展并使用

### HTML 部分

```html
<div class="layui-form">
    <div class="layui-form-item">
        <div class="layui-form-label">排序号</div>
        <div class="layui-input-block">
            <!-- 给输入框配置一个ID -->
            <!-- 注意! 本扩展不影响你使用原生的属性, 如 readonly -->
            <input type="text" placeholder="序号" class="layui-input" id="sort" />
        </div>
    </div>
</div>
```

### Javascript 部分

```javascript
// 引入扩展
layui.use(["numberInput"], function () {
    // 调用扩展
    var numberInput = layui.numberInput;
    // 使用
    numberInput.init("#sort");
});
```

---

# 可配置项目

> 以下配置下都是有默认值的, 你可以在 `numberInput.init(id,opts)` 的第二个参数中使用! 即 `opts`.

## 小技巧

大部分可配置项在 `v1.5.20210609` 开始都可以通过给 DOM 添加 dataset 来配置, 没关系, 我换句人话来说, 就是从 `v1.5.20210609` 开始, 你可以直接通过如下方式设置属性了!

```html
<input type="text" placeholder="序号" class="layui-input" id="sort" data-max="999" data-min="0" data-step="1" />
```

注意 `data-max="999"`, `data-min="0"`,`data-step="1"` 就好了! 如此设置等价于如下设置

```javascript
layui.use(["numberInput"], function () {
    // 调用扩展
    var numberInput = layui.numberInput;
    // 使用
    numberInput.init("#sort", {
        max: 999,
        min: 0,
        step: 1,
    });
});
```

!> 注意! `js` 的优先级要高于 `dataset` 的优先级! 换言之就是如果你设置了同样的属性, 生效的是 `js` 里面配置的值!

!> 还有! 如果配置项不是一个单词, 譬如 `allowEmpty` = `allow` + `Empty`, 如果你想通过 `dataset` 设置, 那么就必须改为 `allow-empty`!

## 可配置项

### max

-   最大值
-   默认值:
    -   在 `v1.0.20210530` 版本为: **999999999**
    -   在 `v1.1.20210602` 版本改为: **19961005**
    -   仔 `v1.5.20210609` 版本恢复为 **999999999**
-   不要问我为啥这个值改动了这么多次!
-   先立个 flag! 我再也不改这个的默认值了, 嘤嘤嘤!

### min

-   最小值
-   默认: **0**

### precision

-   精度
-   默认: 0 `即整数`
-   换句人话讲就是小数点后保留几位

### step

-   步数
-   默认: 动态计算的, 基准值为 1
-   计算公式: 步数 / 比率

#### ratio

!> 注意! 这个不是参数, 只是结合 `step` 说明一下

-   比例
-   比例计算公式为: 10 的 `精度` 次幂
-   换人话将就是
    -   精度=0; 比例=1, step 最小值为 1
    -   精度=1; 比例=10, step 最小值为 0.1
    -   以此类推...

### skin

-   皮肤
-   如果你需要自定义样式
-   那么就可以修改这个字段, 默认: `default`
-   具体什么样式对应什么 DOM 按 F12 开控制台自己调吧!

### iconAdd

-   添加的图标
-   通常情况下特指字体图标
-   默认: `layui-icon layui-icon-addition`

### iconSubtract

-   减少的图标
-   通常情况下特指字体图标
-   默认: `layui-icon layui-icon-subtraction`

### width

-   宽度
-   默认: 100

### defaultValue

!> 注意! 由于 `input` 本身自带 `value` 属性, 如果你同时填写了 `value` 和 `data-default-value`, 那么, `value` 的优先级要比 `data-default-value` 高!

-   默认值
-   默认: 关联输入框的值, 如果不存在则为 0
-   在 `v1.5.20210609` 版本增加

### allowEmpty

-   是否允许空?
-   默认: false
-   如果设置为 true, 当输入框失去焦点的时候不会重置默认值

### autoSelect

-   是否自动全选
-   默认: false
-   如果设置为 true, 则当用户点击了输入框会自动全选!

# 事件

!> 如果你准备了解一下事件, 那么请确保扩展版本大于等于 `v1.1.20210602`, 因为在该版本之前, 事件托管写的比较 **随心情**, 懂得都懂...

## 基本格式

```javascript
layui.use(["numberInput", "layer"], function () {
    var numberInput = layui.numberInput;
    var layer = layui.layer;

    numberInput.init("#sort", {
        // 事件通常都写在 event 下
        // 注意! 你并不是非要全部写上
        // 你只要根据自己的实际需要使用即可!
        event: {
            // 生命周期类
            beforeCreated: function () {},
            created: function () {},
            mounted: function () {},
            // 事件监听类
            change: function () {},
            input: function () {},
            blur: function () {},
            toMin: function () {},
            toMax: function () {},
            focus: function () {},
            select: function () {},
            keypress: function () {},
            mousewheel: function () {},
        },
    });
});
```

## 生命周期类

!> 传递参数中 `DOM树` 是指扩展创建的虚拟 DOM, 如 `输入框, 加减按钮, 包裹容器..等`

![参考配图](./imgs/domTree.png)

### beforeCreated

-   插件开始工作, 但是虚晃一枪, DOM 还没有创建
-   不传递任何参数

### created

-   这回 DOM 已经创建了, 但是还没塞到页面上去
-   可以简单理解成现在 DOM 还是虚拟的
-   传递参数: DOM 树

### mounted

-   已经将 DOM 塞到页面上了
-   传递参数: DOM 树

## 事件监听类

!> 事件监听统一传递参数格式为: `原始Event,谁触发了这个时间,新值是啥,DOM树`, 具体结合上下理解即可!

!> 事件内传递的值都是处理后的值, 跟你输入的值在大部分场景下都没有任何关系! 不要简单理解成, 传递的值=你最开始输入的值!

!> 目前, 事件仅用于监听从而实现业务上特色需求, 暂时无法覆盖本身的处理里逻辑, 举个例子, 你不能利用扩展给你传递 `input` 事件通过比如 `return false` 的方式阻止扩展自身的处理逻辑. 如果你真的需要 `硬改` 扩展默认逻辑, 你 **或许** 可以通过 `DOM树` 选中指定的 DOM 来重写逻辑, 但是很显然我并不推荐你这么做...

### change

-   输入框的值改变了

### input

-   当输入框由内容被输入

### blur

-   当输入框失去了焦点

### toMin

-   当到达最小值

### toMax

-   当到达最大值

### focus

-   当输入框获得焦点

### select

-   当输入框全选

### keypress

-   当按下了某键

### mousewheel

-   当鼠标滚轮滚动

---

# 支持作者

最简单的粗暴的方式就是直接使用 **钞能力**, 当然这是您自愿的! **点击可直接查看大图**

## 钞能力通道

<img src="./imgs/zhifubao.jpg" height="200px" title="支付宝">
<img src="./imgs/weixin.png" height="200px" title="微信">
<img src="./imgs/qq.png" height="200px" title="QQ">

当然, 如果客观条件不允许, 主观上不愿意, 也无伤大雅嘛! 谁的钱都是自己幸苦挣来的. 除了使用 **钞能力**, 你还可以通过以下方式支持作者!

## 打工人通道

1. 给项目点个 Star, 让更多的小伙伴知道这个扩展!
2. 积极测试, 反馈 BUG, 如果发现代码中有不合理的地方积极反馈!
3. 加入粉丝群, 看看有多少志同道合的小伙伴! <a target="_blank" href="https://qm.qq.com/cgi-bin/qm/qr?k=SykXsUtejaedB0TSs6a0_S-SseGByzdU&jump_from=webapi"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="码云 Layui Exts 交流群" title="码云 Layui Exts 交流群"></a>
